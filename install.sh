#!/bin/sh

USER_NAME=$(/usr/bin/who am i | awk '{ print $1 }')
BOOT_CONFIG="/boot/config.txt"
DTPARAMS="i2c1 i2c_arm"


#Adding in /etc/modules
echo "Adding I2C-dev and SPI-dev in /etc/modules . . ."

if grep -q "i2c-dev" /etc/modules; then
  echo "I2C-dev already present"
else
  echo i2c-dev >> /etc/modules
  echo "I2C-dev added"
fi

if grep -q "i2c-bcm2708" /etc/modules; then
  echo "i2c-bcm2708 already present"
else
  echo i2c-bcm2708 >> /etc/modules
  echo "i2c-bcm2708 added"
fi

if grep -q "spi-dev" /etc/modules; then
  echo "spi-dev already present"
else
  echo spi-dev >> /etc/modules
  echo "spi-dev added"
fi

echo "creating dir and file!"
mkdir -p "/boot"
touch "/boot/config.txt"

for i in ${DTPARAMS}
  do
   if grep -q "^dtparam=${i}=on$" ${BOOT_CONFIG}; then
      echo "${i} already present"
   else
      echo "dtparam=${i}=on" >> /boot/config.txt
   fi
done

#adduser ${USER_NAME} i2c
