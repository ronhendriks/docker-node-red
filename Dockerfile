# Container with node-red for raspberry pi with a focus on BLE
# Based on known sources and most maintained packages as possible
#
# Due to errors in Node libraries only version 8 of Node is possible
# This container need the --host=net option to access the bluetooth hardware
# Check working of local bluetooth before using inside container
# 
# example to start container:
# --------------------------
# sudo docker run --net=host -it -p 1880:1880 --name mynodered node-red
#

# we use version 8 because latest give problems with node addons
FROM arm32v7/node:8-jessie 

USER root

# add support for gpio library
RUN apt-get -q update  && apt-get upgrade -qy  
RUN apt-get install -qy \
	python3-dev \
        python3-pip \
	python3 \
	python-smbus \
        gcc \
        bluetooth \
        bluez \
        libbluetooth-dev \
        libudev-dev \
        build-essential \
        bluez-tools \
        libcap2-bin \
        libboost-thread-dev \
        libboost-python-dev \
        libglib2.0-dev \
        python-setuptools \       
        libkrb5-dev \
        openssl \
	avrdude \
        git \
	i2c-tools \
&& rm -rf /var/lib/apt/lists/* \
&& apt-get clean

# Home directory for Node-RED application source code.
RUN mkdir -p /usr/src/node-red

WORKDIR /usr/src/node-red

RUN pip3 install wheel		# needed to suppress error: invalid command 'bdist_wheel'
RUN pip3 install rpi.gpio 
RUN pip3 install pybluez 
RUN pip3 install gattlib

RUN npm install noble	# required to access bluetooth devices

# Home directory for Node-RED application source code.
RUN mkdir -p /usr/src/node-red

# User data directory, contains flows, config and nodes.
RUN mkdir /data

WORKDIR /usr/src/node-red

# Add node-red user so we aren't running as root.
RUN useradd --home-dir /usr/src/node-red --no-create-home node-red \
    && chown -R node-red:node-red /data \
    && chown -R node-red:node-red /usr/src/node-red

# set security for bluetooth
RUN usermod -a -G bluetooth node-red 
RUN setcap cap_net_raw+eip /usr/local/bin/node
RUN setcap cap_net_raw+ep /usr/bin/hcitool	# needed for hcitool to access bluetooth

# add stuff for GrovePi shield
RUN pip3 install RPi.GPIO

#allow user access to i2c
#RUN chgrp i2c /dev/i2c-1
#RUN chmod 666 /dev/i2c-1
RUN usermod -a -G i2c node-red 

# package.json contains Node-RED NPM module and node dependencies
COPY install.sh /usr/src/node-red/
RUN chmod +x /usr/src/node-red/install.sh
RUN /usr/src/node-red/install.sh

##### TODO: want to change to this user, but cant access i2c after this
##### USER node-red

COPY package.json /usr/src/node-red/
RUN npm install

# User configuration directory volume
VOLUME /data
#VOLUME ["/data"]
EXPOSE 1880

# Environment variable holding file path for flows configuration
ENV FLOWS=flows.json
#ENV NODE_PATH=/usr/src/node-red/node_modules:/data/node_modules

CMD ["npm", "start", "--", "--userDir", "/data"]

